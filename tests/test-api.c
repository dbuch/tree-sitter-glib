#include <glib.h>
#include <tree-sitter-glib.h>

static void
test_parser (void)
{
  g_autoptr (GtsParser) parser = gts_parser_new ();
  g_autofree gchar *parsed = NULL;
  const gchar *expected;

  gts_parser_set_language (parser, "c");
  parsed = gts_parser_parse_string (parser,
    "gint main (gint argc, gchar *argv[]) {}"
  );

  expected =
           "(translation_unit "
            "(function_definition (type_identifier) "
             "(function_declarator (identifier) "
              "(parameter_list (parameter_declaration (type_identifier) (identifier)) "
               "(parameter_declaration (type_identifier) (pointer_declarator (array_declarator (identifier)))))"
                ") (compound_statement)))";


  g_assert(g_str_equal (parsed, expected));
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add_func ("/tree-sitter-glib/parser", test_parser);

  return g_test_run();
}
