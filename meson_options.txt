option('enable_tracing', type: 'boolean', value: false)
option('enable_profiling', type: 'boolean', value: false)
option('enable_rdtscp', type: 'boolean', value: false)

# Subproject
option('package_subdir', type: 'string',
  description: 'Subdirectory to append to all installed files, for use as subproject'
)

# Support for multiple languages
option('with_introspection', type: 'boolean', value: true)
option('with_vapi', type: 'boolean', value: true)

option('enable_tests',
       type: 'boolean', value: true,
       description: 'Whether to compile unit tests')
