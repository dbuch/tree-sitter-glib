/* gts-parser.h
 *
 * Copyright 2019 Daniel Buch Hansen <boogiewasthere@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <tree_sitter/api.h>

G_BEGIN_DECLS

#define GTS_TYPE_PARSER (gts_parser_get_type())

G_DECLARE_FINAL_TYPE (GtsParser, gts_parser, GTS, PARSER, GObject)

GtsParser *gts_parser_new                    (void);
void       gts_parser_set_language           (GtsParser   *self,
                                              const gchar *language);
void       gts_parser_get_logger             (GtsParser   *self);
void       gts_parser_set_logger             (GtsParser   *self);
void       gts_parser_parse                  (GtsParser   *self,
                                              const gchar *code);
gchar     *gts_parser_parse_string           (GtsParser   *self,
                                              const gchar *code);
void       gts_parser_parse_text_buffer      (GtsParser   *self);
void       gts_parser_parse_text_buffer_sync (GtsParser   *self);
void       gts_parser_print_dot_graphs       (GtsParser   *self);

G_END_DECLS
