/* gts-parser.c
 *
 * Copyright 2019 Daniel Buch Hansen <boogiewasthere@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "gts-parser.h"

TSLanguage *tree_sitter_c();
TSLanguage *tree_sitter_json();

struct _GtsParser
{
  GObject parent_instance;
};

typedef struct
{
  TSParser *parser;
} GtsParserPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GtsParser, gts_parser, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_LANGUAGE,
  N_PROPS
};

//static GParamSpec *properties [N_PROPS];

GtsParser *
gts_parser_new (void)
{
  return g_object_new (GTS_TYPE_PARSER, NULL);
}

static void
gts_parser_finalize (GObject *object)
{
  GtsParser *self = (GtsParser *)object;
  GtsParserPrivate *priv = gts_parser_get_instance_private (self);

  ts_parser_delete (priv->parser);

  G_OBJECT_CLASS (gts_parser_parent_class)->finalize (object);
}

static void
gts_parser_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  //GtsParser *self = GTS_PARSER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gts_parser_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  //GtsParser *self = GTS_PARSER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gts_parser_class_init (GtsParserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gts_parser_finalize;
  object_class->get_property = gts_parser_get_property;
  object_class->set_property = gts_parser_set_property;
}

static const TSLanguage*
map_ts_language (const gchar *language_name)
{
  const TSLanguage *language = NULL;

  if (g_str_equal (language_name, "c")) {
    language = tree_sitter_c();
  } else if (g_str_equal (language_name, "json")) {
    language = tree_sitter_json();
  } else if (g_str_equal (language_name, "rust")) {
    language = tree_sitter_c();
  } else if (g_str_equal (language_name, "javascript")) {
    language = tree_sitter_c();
  } else if (g_str_equal (language_name, "python")) {
    language = tree_sitter_c();
  } else {
     g_print("Unknow %s language\n", language_name);
     return NULL;
  }

  return language;
}

static void
gts_parser_init (GtsParser *self)
{
  GtsParserPrivate *priv = gts_parser_get_instance_private (self);

  priv->parser = ts_parser_new();
}

void
gts_parser_set_language(GtsParser *self, const gchar *language_name)
{
  GtsParserPrivate *priv = gts_parser_get_instance_private (self);
  const TSLanguage *language = map_ts_language (language_name);

  g_assert(language != NULL);

  ts_parser_set_language (priv->parser, language);
}
void
gts_parser_parse(GtsParser *self, const gchar *code)
{

}

gchar*
gts_parser_parse_string(GtsParser *self, const gchar *code)
{
  GtsParserPrivate *priv = gts_parser_get_instance_private (self);
  gsize len = strlen(code);
  TSTree *tree = ts_parser_parse_string(priv->parser, NULL, code, len);
  TSNode root = ts_tree_root_node (tree);
  return ts_node_string (root);
}
